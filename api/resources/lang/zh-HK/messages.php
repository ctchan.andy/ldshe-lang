<?php

return [
    'design_reviewed' => "|Design has been Approved or Denied.",
    'collection_reviewed' => "|Pattern has been Approved or Denied.",
    'user_not_found' => "|User not found.",
    'group_not_found' => "|Group not found.",
    'member_already' => "|User is in the member list.",
    'member_dropped' => "|You are not in the member list.",
    'member_accepted' => "|You have accepted the invitation before.",
    'member_rejected' => "|You have rejected the invitation before.",
    'member_expired' => "|Invitation expired.",
    'member_left' => "|You have left the Group.",
    'mutex_locked_by' => "|The resource is currently locked by :fname :lname.",
    'mutex_not_found' => "|Lock is missing.",
    'mutex_takenover' => "|:fname :lname has currently taken over the edit right.",
];
