<?php

return [

	'GOBACK_TEXT' => "Go Back",
	'UPDATE_TEXT' => "Update",
	'CANCEL_TEXT' => "Cancel",
	'CLOSE_TEXT'  => "Close",

    // Signup
	'SIGNUP_TEXT'      => "Register",
	'SIGNUP_TEXT2'     => "Sign Up",

    // Signin
    'SIGNIN_FAIL'      => "** FAILED LOGIN **",
    'SIGNIN_TITLE'     => "Please Log In",
    'SIGNIN_TEXT'      => "Login",

    //Navigation
	'NAVTOP_HOME'       => "Home",
	'NAVTOP_ACCOUNT'    => "Account",
	'NAVTOP_ABOUT'		=> "About",
	'NAVTOP_HELP'		=> "Help",
	'NAVTOP_LOGIN'		=> "Login",
	'NAVTOP_LOGOUT'		=> "Logout",
	'NAVTOP_TOGGLE'		=> "Toggle navigation",

	'NAVTOP_ABOUTMENU_CONCEPTUAL'     => "Conceptual Underpinnings",
	'NAVTOP_ABOUTMENU_GUIDE'          => "User guide",
	'NAVTOP_ABOUTMENU_PUBLICATIONS'   => "Publications",
	'NAVTOP_ABOUTMENU_TEAM'           => "<b>LDS<sup>HE</sup></b> Team Members",
	'NAVTOP_ABOUTMENU_ALLIANCE'       => "HKUST MIT Research Alliance Consortium",

	'NAVTOP_HELPMENU_LANGUAGE'      => "Language",
	'NAVTOP_HELPMENU_RESETPWD'      => "Forgot Password",
	'NAVTOP_HELPMENU_REACTIVATION'  => "Resend Activation Email",

	'NAVTOP_ADMINMENU_DASHBOARD'       => "Admin Dashboard",
	'NAVTOP_ADMINMENU_USERMANAGEMENT'  => "User Management",
	'NAVTOP_ADMINMENU_USERPERMISSIONS' => "User Permissions",
	'NAVTOP_ADMINMENU_SYSPAGES'        => "System Pages",
	'NAVTOP_ADMINMENU_MESSAGESADMIN'   => "Messages Admin",
	'NAVTOP_ADMINMENU_SYSLOGS'         => "System Logs",
	'NAVTOP_ADMINMENU_PAGEPERMISSIONS' => "Page Permissions",
	'NAVTOP_ADMINMENU_PAGEMANAGEMENT'  => "Page Management",
	'NAVTOP_ADMINMENU_MESSAGESYS'      => "Message System",

	'NAVTOP_DESIGNMENU_MYDESIGNS'        => "My Designs",
	'NAVTOP_DESIGNMENU_NEWDESIGN'        => "New Design",
	'NAVTOP_DESIGNMENU_PUBLICDESIGNS'    => "Public Designs",
	'NAVTOP_DESIGNMENU_SHAREDDESIGNS'    => "Shared Designs",
	'NAVTOP_DESIGNMENU_TEMPLATEDDESIGNS' => "Templates",

	'NAVTOP_PATTERN'                     => "Pattern Library",
	'NAVTOP_PATTERNMENU_MYPATTERNS'      => "My Patterns",
	'NAVTOP_PATTERNMENU_PUBLICPATTERNS'  => "Public Patterns",
	'NAVTOP_PATTERNMENU_SHAREDPATTERNS'  => "Shared Patterns",

	'NAVTOP_CONTRIBUTIONS'               => "Contributions",
	'NAVTOP_CONTRIBUTIONS_DESIGNS'       => "Contributed Designs",
	'NAVTOP_CONTRIBUTIONS_PATTERNS'      => "Contributed Patterns",

	'NAVTOP_GROUPMENU_MYGROUPS'         => "My Groups",

    //Account
	'ACCOUNT_PERMISSION_ADDED'				=> "Added access to :m1 permission levels",
	'ACCOUNT_PERMISSION_REMOVED'			=> "Removed access from :m1 permission levels",
	'CAPTCHA_ERROR'							=> "You failed the Captcha Test, Robot!",
	'USER_DEV_OPTION'						=> "User is :m1 a dev user",

    //Miscellaneous
	'SQL_ERROR'             => "Fatal SQL error",
	'PAGE_PRIVATE_TOGGLED'  => "This page is now :m1",
	'PAGE_ACCESS_REMOVED'   => "Page access removed for :m1 permission level(s)",
	'PAGE_ACCESS_ADDED'     => "Page access added for :m1 permission level(s)",
	'PAGE_REAUTH_TOGGLED'   => "This page :m1 verification",
	'PAGE_RETITLED'         => "This page has been retitled to ':m1'",

    'MESSAGE_ARCHIVE_SUCCESSFUL'     => "You have successfully archived :m1 threads",
    'MESSAGE_UNARCHIVE_SUCCESSFUL'   => "You have successfully unarchived :m1 threads",
    'MESSAGE_DELETE_SUCCESSFUL'      => "You have successfully deleted :m1 threads",
    'USER_MESSAGE_EXEMPT'            => "User is :m1 exempted from messages.",

    //Permissions
	'PERMISSION_DELETIONS_SUCCESSFUL' => "Successfully deleted :m1 permission level(s)",
	'PERMISSION_REMOVE_PAGES'         => "Successfully removed access to :m1 page(s)",
	'PERMISSION_ADD_PAGES'            => "Successfully added access to :m1 page(s)",
	'PERMISSION_REMOVE_USERS'         => "Successfully removed :m1 user(s)",
	'PERMISSION_ADD_USERS'            => "Successfully added :m1 user(s)",
	'CANNOT_DELETE_NEWUSERS'          => "You cannot delete the default 'new user' group",
	'CANNOT_DELETE_ADMIN'             => "You cannot delete the default 'admin' group",

	//Validate
	'VALIDATE_VERBTOBE'          => "is",
	'VALIDATE_VERBTOBEPLURAL'    => "are",
	'VALIDATE_VERBHAVETOBE'      => "have to be",
	'VALIDATE_FOLLOWING'         => "has to be one of the following",
	'VALIDATE_REQUIRED'          => ":display :verb required",
	'VALIDATE_MINCHARS'          => ":display must be a minimum of :value characters",
	'VALIDATE_MAXCHARS'          => ":display must be a maximum of :value characters",
	'VALIDATE_MATCHES'           => ":display1 and :display2 must match",
	'VALIDATE_UNIQUE'            => ":display already exists. Please choose another :display",
	'VALIDATE_DBERR'             => "Cannot verify :display. Database error",
	'VALIDATE_NUMERIC'           => ":display has to be a number. Please use a numeric value",
	'VALIDATE_EMAIL'             => ":display must be a valid email address",
	'VALIDATE_<'                 => ":display must be smaller than :value",
	'VALIDATE_>'                 => ":display must be larger than :value",
	'VALIDATE_<='                => ":display must be equal :value or smaller",
	'VALIDATE_>='                => ":display must be equal :value or larger",
	'VALIDATE_!='                => ":display must be different from :value",
	'VALIDATE_=='                => ":display must equal :value",
	'VALIDATE_INTEGER'           => ":display has to be an integer",
	'VALIDATE_TIMEZONE'          => ":display has to be a valid time zone name",
	'VALIDATE_DATETIME'          => ":display has to be a valid time",
	'VALIDATE_HELP'              => ":display is not being checked properly by our system.  Please contact us for assistance",
	'VALIDATE_INRANGE'           => ":display is not a valid selection",
	'VALIDATE_PHONE'             => ":display must be a valid North American phone number",

	//Index Page
	'INDEX_WELCOME'       => "Welcome to :sitename",
	'INDEX_DESCRIPTION'   => "A tool to support teaching professionals in the design and implementation of fully online and blended courses.",
	'INDEX_ACCOUNT'       => "User Account",

	//About Page
	'ABOUTGUIDE_INTRODUCTION'     => "Introduction to Learning Design Studio HE",
	'ABOUTGUIDE_COURSE'           => "Course Level",
	'ABOUTGUIDE_COMPONENTS'       => "Strategic Components and Tasks",
	'ABOUTGUIDE_CREATEPATTERNS'   => "Create Patterns",
	'ABOUTGUIDE_SHAREDPATTERNS'   => "Shared Patterns",
	'ABOUTGUIDE_SESSION'          => "Session Organization",
	'ABOUTGUIDE_DASHBOARD'        => "Designer's Dashboard",
	'ABOUTGUIDE_PRINTABLE'        => "Printable Version",

	'ABOUTTEAM_INVESTIGATOR' => "Principal Investigator",
	'ABOUTTEAM_FELLOW'       => "Postdoctoral Fellow",
	'ABOUTTEAM_MEMBERS'      => "Research team members",

	'ABOUTAlliance_CONSORTIUM' =>  "HKUST MIT Research Alliance Consortium",

	//Login Page
	'SIGNUP_USERNAMEOREMAIL'   => "Username / Email",
	'SIGNIN_CHECKRECAPTCHA'    => "Please check the reCaptcha.",
	'SIGNIN_REMEMBER'          => "Remember Me",
	'SIGNIN_INVALIDCREDENTIAL' => "Please check your username and password and try again.",
	'SIGNIN_RESETPWD'          => "Forgot Password",

	//Register Page
	'SIGNUP_USERNAME'             => "Username",
	'SIGNUP_FIRSTNAME'            => "First Name",
	'SIGNUP_LASTNAME'             => "Last Name",
	'SIGNUP_EMAIL'                => "Email",
	'SIGNUP_PASSWORD'             => "Password",
	'SIGNUP_PICKPASSWORD'         => "Choose a Password",
	'SIGNUP_CONFIRMPASSWORD'      => "Confirm Password",
	'SIGNUP_PASSWORDCRITERIA'     => "Passwords Should...",
	'SIGNUP_SHOWPASSWORD'         => "Show Passwords",
	'SIGNUP_TERMS'                => "Registration User Terms and Conditions",
	'SIGNUP_TERMSDETAILS'         =>
"Welcome to our website. If you continue to browse and use this website, you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern our relationship with you in relation to this website. If you disagree with any part of these terms and conditions, please do not use our website.

The use of this website is subject to the following terms of use:

The content of the pages of this website is for your general information and use only. It is subject to change without notice.

This website uses cookies to monitor browsing preferences. If you do allow cookies to be used, the following personal information may be stored by us for use by third parties.

Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose.

You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.

Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.

This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.

All trade marks reproduced in this website which are not the property of, or licensed to, the operator are acknowledged on the website.

Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.

From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).",
	'SIGNUP_AGREETERMS'           => "Check box to agree to terms",
	'SIGNUP_READTERMS'            => "Please read and accept terms and conditions",
	'SIGNUP_CHECKRECAPTCHA'       => "Please check the reCaptcha box.",
	'SIGNUP_CHARSBETWEEN'         => "Be between :min and :max characters",
	'SIGNUP_CAPSMIN'              => "Have at least :min capital letter",
	'SIGNUP_NUMMIN'               => "Have at least :min number",
	'SIGNUP_CHARSRETYPED'         => "Be typed correctly twice",
	'SIGNUP_THANKS'               => "Thanks for registering!",

	//Forgot Password Page
	'RESETPWD_TITLE'           => "Reset your password.",
	'RESETPWD_INSTRUCT1'       => "Enter your email address and click Reset.",
	'RESETPWD_INSTRUCT2'       => "Check your email and click the link that is sent to you.",
	'RESETPWD_INSTRUCT3'       => "Follow the on screen instructions.",
	'RESETPWD_RESET'           => "Reset",
	'RESETPWD_RESET2'          => "Reset Password",
	'RESETPWD_RESETTITLE'      => "Please reset your password.",
	'RESETPWD_NEWPASSWORD'     => "New Password",
	'RESETPWD_CONFIRMPASSWORD' => "Confirm Password",
	'RESETPWD_SUCCESS'         => "Your password has been reset!",
	'RESETPWD_ERROR'           => "Oops...something went wrong, maybe an old reset link you clicked on. Click below to try again!",

	'RESETPWD_TPL_SUBJECT'     => "Password Reset",
	'RESETPWD_TPL_GREETING'    => "Hello :fname,",
	'RESETPWD_TPL_TOPIC'       => "You are receiving this email because a request was made to reset your password. If this was not you, you may disregard this email.",
	'RESETPWD_TPL_INSTRUCT'    => "If this was you, click the link below to continue with the password reset process.",
	'RESETPWD_TPL_RESETLINK'   => "Reset Password",
	'RESETPWD_TPL_CLOSING'     => "Sincerely,",
	'RESETPWD_TPL_SENDER'      => "-The Team-",
	'RESETPWD_TPL_NOTE'        => "Please note, Password links expire in :expiry minutes.",
	'RESETPWD_TPL_NOTSENT'     => "Email NOT sent due to error. Please contact site administrator.",
	'RESETPWD_TPL_NOTEXISTS'   => "That email does not exist in our database",
	'RESETPWD_TPL_SENT'        => "Your password reset link has been sent to your email address.",
	'RESETPWD_TPL_REMIND'      => "Click the link in the email to Reset your password. Be sure to check your spam folder if the email isn't in your inbox.",
	'RESETPWD_TPL_EXPIRYIN'    => "Reset links are only valid for :expiry minutes.",

	//Account Page
	'ACC_EDITINFO'        => "Edit Account Info",
	'ACC_PROFILE'         => "Public Profile",
	'ACC_SIGNUPDATE'      => "Member Since",
	'ACC_LOGINNO'         => "Number of Logins",

	//User Settings Page
	'USETTINGS_TITLE'                 => "Update your user settings",
	'USETTINGS_UPDATED'               => "Account Updated",
	'USETTINGS_USERNAMEUPDATED'       => "Username updated.",
	'USETTINGS_FIRSTNAMEUPDATED'      => "First name updated.",
	'USETTINGS_LASTNAMEUPDATED'       => "Last name updated.",
	'USETTINGS_EMAILUPDATED'          => "Email updated.",
	'USETTINGS_EMAILNOTMATCHED'       => "Your email did not match.",
	'USETTINGS_PWDUPDATED'            => "Password updated.",
	'USETTINGS_REUSEOLDPWD'           => "Your old password cannot be the same as your new password",
	'USETTINGS_PWDUPDATEFAILED'       => "Current password verification failed. Update failed. Please try again.",
	'USETTINGS_PROFILEPICHINT'        => "Want to change your profile picture?",
	'USETTINGS_PROFILEPICINSTRUCT'    => "Visit :link and setup an account with the email address :email. It works across millions of sites. It's fast and easy!",
	'USETTINGS_USERNAMECANTCHANGE'        => "Why can't I change this?",
	'USETTINGS_USERNAMECANTCHANGEREASON'  => "The Administrator has disabled changing usernames.",
	'USETTINGS_CONFIRMEMAIL'          => "Confirm Email",
	'USETTINGS_OLDPASSWORD'           => "Old Password :desc",
	'USETTINGS_OLDPASSWORDDESC'       => "(Required for changing password, email, or resetting PIN)",
	'USETTINGS_PASSWORDTIPS'          => ":min char min, :max max.",
	'USETTINGS_CONFIRMPASSWORDTIPS'   => "Must match the New Password",
	'USETTINGS_OLDPASSWORDDTIPS'      => "Required to change your password",

	//User Profile Page
	'UPROFILE_BIO'           => "Bio",
	'UPROFILE_EDITBIO'       => "Edit Bio",
	'UPROFILE_WHOSEPROFILE'  => ":username 's Profile",
	'UPROFILE_USERNOTFOUND'  => "User not found",
	'UPROFILE_GOHOME'        => "Go to the homepage",
	'UPROFILE_ALLUSERS'      => "All Users",
	'UPROFILE_UPDATE'        => "Update Bio",

	//View All User Page
	'VIEWALL_TITLE'          => "View All Users",
	'VIEWALL_SEARCH'         => "Search",
	'VIEWALL_SEARCHUSER'     => "Search Users...",
	'VIEWALL_SEARCHING'      => "Searching for&#58; :data",
	'VIEWALL_SEARCHNOTFOUND' => "No entries found.",

	//Footer
	'COPYRIGHT' => "All rights reserved, CITE, HKU <br/> This project is part of the HKUST MIT Research Alliance Consortium project \"An Open Learning Design, Data Analytics & Visualization Framework for E-Learning (ITS/306/15FP)\", funded by the Innovative Technology Fund of the HKSAR.",

	//Token error
	'TOKENERR_RESUBMIT'      => "There was an error with your form. Please go back and try again. Please note that submitting the form by refreshing the page will cause an error.",
	'TOKENERR_CONTACTADMIN'  => "If this continues to happen, please contact the administrator.",
];
