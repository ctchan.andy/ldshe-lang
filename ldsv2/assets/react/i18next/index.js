import i18next from 'i18next';
import {reactI18nextModule} from "react-i18next";
import en from './en';
import zh_HK from './zh_hk';
import zh_CN from './zh_cn';

i18next
    .use(reactI18nextModule)
    .init({
        lng: 'en',
        fallbackLng: 'en',
        // debug: true,
        interpolation: {
            // html escape is not needed for react as it escapes by default
            escapeValue: false,
        },
        resources: {
            en,
            'zh-HK': zh_HK,
            'zh-TW': zh_HK,
            'zh-CN': zh_CN,
        },
    });

export default i18next;
